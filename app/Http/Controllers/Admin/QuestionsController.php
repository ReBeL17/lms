<?php

namespace App\Http\Controllers\Admin;

use App\Quiz;
use App\Course;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyQuestionRequest;
use App\Http\Requests\StoreQuestionRequest;
use App\Http\Requests\UpdateQuestionRequest;
use App\Question;
use App\Option;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class QuestionsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        abort_if(Gate::denies('question-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $questions = Question::whereHas('quizzes', function ($q) {
                        $q->whereIn('quiz_id', Quiz::ofTeacher()->pluck('id'));
                    });
        // $questions = Question::get();
        // dd($questions);
        if (request('show_deleted') == 1) {
            abort_if(Gate::denies('question-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
            $questions = $questions->onlyTrashed()->get();
        } else {
            $questions = $questions->get();
        }
        return view('admin.questions.index', compact('questions'));
    }

    public function create()
    {
        abort_if(Gate::denies('question-create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $quizzes = Quiz::ofTeacher()->pluck('title', 'id')->prepend(trans('global.pleaseSelect').' a Quiz', '');
        
        return view('admin.questions.create', compact('quizzes'));
    }

    public function store(StoreQuestionRequest $request)
    {
        // dd($request->all());
        $data1 = [
            'question_text' => $request->question_text,
            // 'quiz_id' => $request->quiz_id,
        ];
        $question = Question::create($data1);
        $question->quizzes()->sync($request->quiz_id);
        //inserting data into options table.
        $options = $request->option_text;
        $point = $request->points;
        foreach($options as $key => $option){
        $data2 = [
            'option_text' => $option,
            'points' => (($key+1)==$point)?1:0,
            'question_id' => $question->id,
        ];
            Option::create($data2);
        }
        return redirect()->route('admin.questions.index');
    }

    public function edit(Question $question)
    {
        abort_if(Gate::denies('question-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $quizzes = Quiz::all()->pluck('title', 'id')->prepend(trans('global.pleaseSelect').' a quiz', '');

        $question->load('quizzes');
        
        return view('admin.questions.edit', compact('quizzes', 'question'));
    }

    public function update(UpdateQuestionRequest $request, Question $question)
    {
       
       $data1 = [
                    'question_text' => $request->question_text,
                   
                ];
        $question->update($data1);
        $question->quizzes()->sync($request->quiz_id);
                
        //inserting data into options table.
        $options = $request->option_text;
        $point = $request->points;
        $prevOptions = $question->questionOptions->pluck('id');
        
        if(count($options) == count($prevOptions)) {
            foreach($options as $key => $option) {
            
                $data2 = [
                    'option_text' => $option,
                    'points' => (($key+1)==$point)?1:0,
                    'question_id' => $question->id,
                ];

                Option::updateOrInsert(
                    ['question_id'=>$question->id, 'id'=>$prevOptions[$key] ],
                    $data2
                );
            }
        } else {
            Option::where('question_id',$question->id)->delete();
            foreach($options as $key => $option) {
            
                $data2 = [
                    'option_text' => $option,
                    'points' => (($key+1)==$point)?1:0,
                    'question_id' => $question->id,
                ];
                Option::create($data2);
            }
        }
        
        return redirect()->route('admin.questions.index');
    }

    public function show(Question $question)
    {
        abort_if(Gate::denies('question-show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $question->load('quizzes');

        return view('admin.questions.show', compact('question'));
    }

    public function destroy(Question $question)
    {
        abort_if(Gate::denies('question-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $question->delete();

        return back();
    }

    public function massDestroy(MassDestroyQuestionRequest $request)
    {
        Question::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

     /**
     * Restore Test from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        abort_if(Gate::denies('question-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $question = Question::onlyTrashed()->findOrFail($id);
        $question->restore();

        return redirect()->route('admin.questions.index');
    }

    /**
     * Permanently delete question from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        abort_if(Gate::denies('question-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $question = Question::onlyTrashed()->findOrFail($id);
        $question->forceDelete();

        return redirect()->route('admin.questions.index');
    }
}
