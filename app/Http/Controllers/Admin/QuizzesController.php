<?php

namespace App\Http\Controllers\Admin;

use App\Quiz;
use App\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTestsRequest;
use App\Http\Requests\UpdateTestsRequest;
use Symfony\Component\HttpFoundation\Response;

class QuizzesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of Test.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        abort_if(Gate::denies('quiz-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        
        if (request('show_deleted') == 1) {
            abort_if(Gate::denies('quiz-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
            $quizzes = Quiz::onlyTrashed()->ofTeacher()->get();
        } else {
            $quizzes = Quiz::ofTeacher()->get();
        }

        return view('admin.quizzes.index', compact('quizzes'));
    }

    /**
     * Show the form for creating new Test.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_if(Gate::denies('quiz-create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $courses = \App\Course::ofTeacher()->get();
        $courses_ids = $courses->pluck('id');
        $courses = $courses->pluck('title', 'id')->prepend('Please select', '');
        $lessons = \App\Lesson::whereIn('course_id', $courses_ids)->get()->pluck('title', 'id')->prepend('Please select', '');
        $teachers = \App\Admin::whereHas('roles', function ($q) { $q->where('role_id', 3); } )->get()->pluck('name', 'id');
        return view('admin.quizzes.create', compact('courses', 'lessons','teachers'));
    }

    /**
     * Store a newly created Test in storage.
     *
     * @param  \App\Http\Requests\StoreTestsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTestsRequest $request)
    {
        abort_if(Gate::denies('quiz-create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        // dd($request->all());
        $test = Quiz::create($request->all());
        $teachers = \Auth::user()->isAdmin() ? array_filter((array)$request->input('teachers')) : [\Auth::user()->id];
        $test->teachers()->sync($teachers);
        return redirect()->route('admin.quizzes.index');
    }


    /**
     * Show the form for editing Test.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort_if(Gate::denies('quiz-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $courses = \App\Course::ofTeacher()->get();
        $courses_ids = $courses->pluck('id');
        $courses = $courses->pluck('title', 'id')->prepend('Please select', '');
        $lessons = \App\Lesson::whereIn('course_id', $courses_ids)->get()->pluck('title', 'id')->prepend('Please select', '');
        $teachers = \App\Admin::whereHas('roles', function ($q) { $q->where('role_id', 3); } )->get()->pluck('name', 'id');
        $test = Quiz::findOrFail($id);

        return view('admin.quizzes.edit', compact('test', 'courses', 'lessons','teachers'));
    }

    /**
     * Update Test in storage.
     *
     * @param  \App\Http\Requests\UpdateTestsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTestsRequest $request, $id)
    {
        abort_if(Gate::denies('quiz-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $test = Quiz::findOrFail($id);
        $test->update($request->all());
        $teachers = \Auth::user()->isAdmin() ? array_filter((array)$request->input('teachers')) : [\Auth::user()->id];
        $test->teachers()->sync($teachers);
        return redirect()->route('admin.quizzes.index');
    }


    /**
     * Display Test.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort_if(Gate::denies('quiz-show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $test = Quiz::findOrFail($id);

        return view('admin.quizzes.show', compact('test'));
    }


    /**
     * Remove Test from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort_if(Gate::denies('quiz-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $test = Quiz::findOrFail($id);
        $test->delete();

        return redirect()->route('admin.quizzes.index');
    }

    /**
     * Delete all selected Test at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        abort_if(Gate::denies('quiz-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        if ($request->input('ids')) {
            $entries = Quiz::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Test from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        abort_if(Gate::denies('quiz-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $test = Quiz::onlyTrashed()->findOrFail($id);
        $test->restore();

        return redirect()->route('admin.quizzes.index');
    }

    /**
     * Permanently delete Test from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        abort_if(Gate::denies('quiz-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $test = Quiz::onlyTrashed()->findOrFail($id);
        $test->forceDelete();

        return redirect()->route('admin.quizzes.index');
    }
}
