<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $permissions = [
            [
                'id'    => '1',
                'title' => 'user_management_access',
                'slug' => 'user-management-access',
            ],
            [
                'id'    => '2',
                'title' => 'permission_create',
                'slug' => 'permission-create',
            ],
            [
                'id'    => '3',
                'title' => 'permission_edit',
                'slug' => 'permission-edit',
            ],
            [
                'id'    => '4',
                'title' => 'permission_show',
                'slug' => 'permission-show',
            ],
            [
                'id'    => '5',
                'title' => 'permission_delete',
                'slug' => 'permission-delete',
            ],
            [
                'id'    => '6',
                'title' => 'permission_access',
                'slug' => 'permission-access',
            ],
            [
                'id'    => '7',
                'title' => 'role_create',
                'slug' => 'role-create',
            ],
            [
                'id'    => '8',
                'title' => 'role_edit',
                'slug' => 'role-edit',
            ],
            [
                'id'    => '9',
                'title' => 'role_show',
                'slug' => 'role-show',
            ],
            [
                'id'    => '10',
                'title' => 'role_delete',
                'slug' => 'role-delete',
            ],
            [
                'id'    => '11',
                'title' => 'role_access',
                'slug' => 'role-access',
            ],
            [
                'id'    => '12',
                'title' => 'user_create',
                'slug' => 'user-create',
            ],
            [
                'id'    => '13',
                'title' => 'user_edit',
                'slug' => 'user-edit',
            ],
            [
                'id'    => '14',
                'title' => 'user_show',
                'slug' => 'user-show',
            ],
            [
                'id'    => '15',
                'title' => 'user_delete',
                'slug' => 'user-delete',
            ],
            [
                'id'    => '16',
                'title' => 'user_access',
                'slug' => 'user-access',
            ],
            [
                'id'    => '17',
                'title' => 'group_create',
                'slug' => 'group-create',
            ],
            [
                'id'    => '18',
                'title' => 'group_edit',
                'slug' => 'group-edit',
            ],
            [
                'id'    => '19',
                'title' => 'group_show',
                'slug' => 'group-show',
            ],
            [
                'id'    => '20',
                'title' => 'group_delete',
                'slug' => 'group-delete',
            ],
            [
                'id'    => '21',
                'title' => 'group_access',
                'slug' => 'group-access',
            ],
            [
                'id'    => '22',
                'title' => 'course_create',
                'slug' => 'course-create',
            ],
            [
                'id'    => '23',
                'title' => 'course_edit',
                'slug' => 'course-edit',
            ],
            [
                'id'    => '24',
                'title' => 'course_show',
                'slug' => 'course-show',
            ],
            [
                'id'    => '25',
                'title' => 'course_delete',
                'slug' => 'course-delete',
            ],
            [
                'id'    => '26',
                'title' => 'course_access',
                'slug' => 'course-access',
            ],
            [
                'id'    => '27',
                'title' => 'lesson_create',
                'slug' => 'lesson-create',
            ],
            [
                'id'    => '28',
                'title' => 'lesson_edit',
                'slug' => 'lesson-edit',
            ],
            [
                'id'    => '29',
                'title' => 'lesson_show',
                'slug' => 'lesson-show',
            ],
            [
                'id'    => '30',
                'title' => 'lesson_delete',
                'slug' => 'lesson-delete',
            ],
            [
                'id'    => '31',
                'title' => 'lesson_access',
                'slug' => 'lesson-access',
            ],
        ];

        Permission::insert($permissions);
    }
}
