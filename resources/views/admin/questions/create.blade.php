@extends('admin.backend.layouts.master')
@section('title','Add Question')

@section('styles')
 <!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('css/bootstrap/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('css/admin/addQuizform.css') }}">   
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.question.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.questions.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="field-wrapper">
                <label for="quiz_id" placeholder="Please select a quiz">Please select a quiz</label>
                <select class="{{ $errors->has('quiz') ? 'is-invalid' : '' }}" name="quiz_id" id="quiz_id" required> 
                    @foreach($quizzes as $id => $quiz)
                        <option value="{{ $id }}" {{ old('quiz_id') == $id ? 'selected' : '' }}>{{ $quiz }}</option>
                    @endforeach
                </select>
                @if($errors->has('quiz_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('quiz_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.question.fields.quiz_helper') }}</span>
            </div>
            {{-- <div class="field-wrapper">
                <label for="subcategory_id" placeholder="Please select a subcategory">Please select a subcategory</label>
                <select class="{{ $errors->has('subcategory') ? 'is-invalid' : '' }}" name="subcategory_id" id="subcategory_id" required> 
                    
                        <option value="">Please select a subcategory</option>
                    
                </select>
                @if($errors->has('subcategory_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('subcategory_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.question.fields.subcategory_helper') }}</span>
            </div> --}}
            <div class="form-group">
                <textarea class="{{ $errors->has('question_text') ? 'is-invalid' : '' }}" name="question_text" id="question_text" required>{{ old('question_text') }}</textarea>
                <label for="question_text" placeholder="Write your question here" alt="question"></label>
                @if($errors->has('question_text'))
                    <div class="invalid-feedback">
                        {{ $errors->first('question_text') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.question.fields.question_text_helper') }}</span>
            </div>

        <div class="option" id="option">
            <div class="col-md-12 row">
                <div class="icheck-success">
                    <input type="radio" name="points" id="option1" value="1" required>
                    <label for="option1">
                    </label>
                </div>
                <div class="col-md-8">
                <input class="{{ $errors->has('option_text') ? 'is-invalid' : '' }}" name="option_text[]" id="option_text" type="text" value="{{ old('option_text[]', '') }}" required>
                
                <label for="option_text" placeholder="Option 1" alt="option"></label>
                </div>
                @if($errors->has('option_text'))
                    <div class="invalid-feedback">
                        {{ $errors->first('option_text') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.option.fields.option_text_helper') }}</span>
            </div>

            <div class="col-md-12 row">
                <div class="icheck-success">
                    <input type="radio" name="points" id="option2" value="2">
                    <label for="option2">
                    </label>
                </div>
                <div class="col-md-8">
                <input class="{{ $errors->has('option_text') ? 'is-invalid' : '' }}" name="option_text[]" id="option_text" type="text" value="{{ old('option_text[]', '') }}" required>
                <label for="option_text" placeholder="Option 2" alt="option"></label>
                </div>
                @if($errors->has('option_text'))
                    <div class="invalid-feedback">
                        {{ $errors->first('option_text') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.option.fields.option_text_helper') }}</span>
            </div>
        </div>
        <div class="form-group">
           <input type="button" class="btn btn-default" id="add" value="Add more option"> 
        </div>
        {{-- <div class="form-group">
            <textarea class="{{ $errors->has('answer_explanation') ? 'is-invalid' : '' }}" name="answer_explanation" id="answer_explanation" required>{{ old('answer_explanation') }}</textarea>
            <label for="answer_explanation" placeholder="Write explanation of correct answer here" alt="answer_explanation"></label>
            @if($errors->has('answer_explanation'))
                <div class="invalid-feedback">
                    {{ $errors->first('answer_explanation') }}
                </div>
            @endif
            <span class="help-block">{{ trans('cruds.question.fields.answer_explanation_helper') }}</span>
        </div> --}}
    </div>
        <div class="card-footer">
                <div class="form-group">
                    <button class="btn btn-success" type="submit">
                        {{ trans('global.save') }}
                    </button>
                </div>
        </div>
        </form>
    
</div>
@endsection

@section('scripts')
   <script src="{{ asset('js/admin/adaptiveDropdown.js') }}"></script>
   <script type="text/javascript">

        $(document).ready(function(){
            
                var i=$('#option input[type="radio"]').length;
            //adding options dynamically
            $('#add').click(function(){
                i++;
                $('#option').append('<div class="col-md-12 row" id="row'+i+'"><div class="icheck-success"><input type="radio" name="points" id="option'+i+'" value="'+i+'"><label for="option'+i+'"></label></div><div class="col-md-8"><input class="{{ $errors->has('option_text') ? 'is-invalid' : '' }}" name="option_text['+(i-1)+']" id="option_text" type="text" value="{{ old('option_text[]', '') }}" required><label for="option_text" placeholder="New Option" alt="option"></label></div><div class="col-md-1"><button id="'+i+'" class="btn btn-secondary remove" alt="Delete this option"><i class="fas fa-trash"></i></button></div>@if($errors->has('option_text'))<div class="invalid-feedback">{{ $errors->first('option_text') }}</div>@endif<span class="help-block">{{ trans('cruds.option.fields.option_text_helper') }}</span></div>');
                               
                var j = $('#option input[type="radio"]').length;
                console.log(j)
                if(j>=4){
                        $('#add').hide();
                    }
            });
            
            $('body').on('click','.remove',function(){
                var button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
                var j = $('#option input[type="radio"]').length;
                if(j<4){
                            $('#add').show();
                        }
            });                 
        });
    </script>
@endsection

