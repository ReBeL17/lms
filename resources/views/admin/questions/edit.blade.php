@extends('admin.backend.layouts.master')
@section('title','Edit Question')


@section('styles')
 <!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('css/bootstrap/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('css/admin/addQuizform.css') }}">   
@endsection

@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.question.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.questions.update", [$question->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            
            <div class="field-wrapper">
                <label for="quiz_id" placeholder="Please select a quiz">Please select a quiz</label>
                <select class="{{ $errors->has('quiz_id') ? 'is-invalid' : '' }}" name="quiz_id" id="quiz_id" required> 
                    @foreach($quizzes as $id => $quiz)
                        <option value="{{ $id }}" @foreach($question->quizzes as $q){{ ($q->title ? $q->id : old('quiz_id')) == $id ? 'selected' : '' }}@endforeach>{{ $quiz }}</option>
                    @endforeach
                </select>
                @if($errors->has('quiz_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('quiz_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.question.fields.quiz_helper') }}</span>
            </div>
            
            <div class="form-group">
                <textarea class="{{ $errors->has('question_text') ? 'is-invalid' : '' }}" name="question_text" id="question_text" required>{{ $question->question_text , old('question_text') }}</textarea>
                <label for="question_text" placeholder="Write your question here" alt="question"></label>
                @if($errors->has('question_text'))
                    <div class="invalid-feedback">
                        {{ $errors->first('question_text') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.question.fields.question_text_helper') }}</span>
            </div>

        <div class="option" id="option">
            @foreach($question->questionOptions as $key=>$options)
            <div class="col-md-12 row">
                <div class="icheck-success">
                    <input type="radio" name="points" id="option{{ $key+1 }}" value="{{ $key+1 }}"  {{ (($options->points==1) ? 'checked' : '' )}} required>
                    <label for="option{{ $key+1 }}">
                    </label>
                </div>
                <div class="col-md-8">
                <input class="{{ $errors->has('option_text') ? 'is-invalid' : '' }}" name="option_text[]" id="option_text" type="text" value="{{ $options->option_text , old('option_text[]') }}" required>
                
                <label for="option_text" placeholder="Option {{$key+1}}" alt="option"></label>
                </div>
                @if($errors->has('option_text'))
                    <div class="invalid-feedback">
                        {{ $errors->first('option_text') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.option.fields.option_text_helper') }}</span>
            </div>
            @endforeach

        </div>
        
            <input type="button" class="btn btn-default" id="add" value="Add more option"> 
            
    </div>

    <div class="card-footer">
        <div class="form-group">
            <button class="btn btn-danger" type="submit">
                {{ trans('global.update') }}
            </button>
        </div>
    </div>
    </form>
</div>

@endsection

@section('scripts')
   <script src="{{ asset('js/admin/adaptiveDropdown.js') }}"></script>
   <script type="text/javascript">

        $(document).ready(function(){
            
                var i=$('#option input[type="radio"]').length;
                if(i>=4){
                        $('#add').hide();
                    }
            //adding options dynamically
            $('#add').click(function(){
                i++;
                $('#option').append('<div class="col-md-12 row" id="row'+i+'"><div class="icheck-success"><input type="radio" name="points" id="option'+i+'" value="'+i+'"><label for="option'+i+'"></label></div><div class="col-md-8"><input class="{{ $errors->has('option_text') ? 'is-invalid' : '' }}" name="option_text['+(i-1)+']" id="option_text" type="text" value="{{ old('option_text[]', '') }}" required><label for="option_text" placeholder="New Option" alt="option"></label></div><div class="col-md-1"><button id="'+i+'" class="btn btn-secondary remove" alt="Delete this option"><i class="fas fa-trash"></i></button></div>@if($errors->has('option_text'))<div class="invalid-feedback">{{ $errors->first('option_text') }}</div>@endif<span class="help-block">{{ trans('cruds.option.fields.option_text_helper') }}</span></div>');
                               
                var j = $('#option input[type="radio"]').length;
                console.log(j)
                if(j>=4){
                        $('#add').hide();
                    }
            });
            
            $('body').on('click','.remove',function(){
                var button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
                var j = $('#option input[type="radio"]').length;
                if(j<4){
                            $('#add').show();
                        }
            });      
 
        });
    </script>
@endsection