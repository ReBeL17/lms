@extends('admin.backend.layouts.master')
@section('title','Edit Quiz')

@section('content')
<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.quizzes.title_singular') }}
    </div>

    <div class="card-body"> 
            {!! Form::model($test, ['method' => 'PUT', 'route' => ['admin.quizzes.update', $test->id]]) !!}
            <div class="row">
                <div class="col-md-12 form-group">
                    {!! Form::label('course_id', 'Course', ['class' => 'control-label required']) !!}
                    {!! Form::select('course_id', $courses, old('course_id'), ['class' => 'form-control select2']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('course_id'))
                        <p class="help-block">
                            {{ $errors->first('course_id') }}
                        </p>
                    @endif
                </div>
            </div>
            {{-- <div class="row">
                <div class="col-md-12 form-group">
                    {!! Form::label('lesson_id', 'Lesson', ['class' => 'control-label']) !!}
                    {!! Form::select('lesson_id', $lessons, old('lesson_id'), ['class' => 'form-control select2']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('lesson_id'))
                        <p class="help-block">
                            {{ $errors->first('lesson_id') }}
                        </p>
                    @endif
                </div>
            </div> --}}
            @if (Auth::user()->isAdmin())
            <div class="row">
                <div class="col-md-12 form-group">
                    {!! Form::label('teachers', 'Teachers', ['class' => 'control-label row']) !!}
                    {!! Form::select('teachers[]', $teachers, old('teachers') ? old('teachers') : $test->teachers->pluck('id')->toArray(), ['class' => 'form-control select2  col-md-12 row', 'multiple' => 'multiple']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('teachers'))
                        <p class="help-block">
                            {{ $errors->first('teachers') }}
                        </p>
                    @endif
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-md-12 form-group">
                    {!! Form::label('title', 'Title', ['class' => 'control-label required']) !!}
                    {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('title'))
                        <p class="help-block">
                            {{ $errors->first('title') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 form-group">
                    {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
                    {!! Form::textarea('description', old('description'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('description'))
                        <p class="help-block">
                            {{ $errors->first('description') }}
                        </p>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 form-group">
                    {!! Form::label('published', 'Published', ['class' => 'control-label']) !!}
                    {!! Form::hidden('published', 0) !!}
                    {!! Form::checkbox('published', 1, old('published'), []) !!}
                    <p class="help-block"></p>
                    @if($errors->has('published'))
                        <p class="help-block">
                            {{ $errors->first('published') }}
                        </p>
                    @endif
                </div>
            </div>
            
            {!! Form::submit(trans('global.update'), ['class' => 'btn btn-success']) !!}
        </div>
    </div>

    {!! Form::close() !!}
@stop

