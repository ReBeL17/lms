<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');


Route::group(['prefix' => 'admin', 'as' => 'admin.'], function() {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('login');
    Route::get('/', 'Admin\HomeController@index')->name('dashboard');
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('logout');

     //change password
     Route::get('change-password', 'Admin\ChangePasswordController@create')->name('password.create');
     Route::post('change-password', 'Admin\ChangePasswordController@update')->name('password.update');
 
    //password reset routes
    Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('password.reset');

    // Permissions
    Route::delete('permissions/destroy', 'Admin\PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'Admin\PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'Admin\RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'Admin\RolesController');

    // Users
    Route::delete('users/destroy', 'Admin\UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'Admin\UsersController');

    // groups
    Route::delete('groups/destroy', 'Admin\GroupsController@massDestroy')->name('groups.massDestroy');
    Route::resource('groups', 'Admin\GroupsController');

    // Categories
    Route::delete('categories/destroy', 'Admin\CategoriesController@massDestroy')->name('categories.massDestroy');
    Route::resource('categories', 'Admin\CategoriesController');
    Route::post('categories_restore/{id}', ['uses' => 'Admin\CategoriesController@restore', 'as' => 'categories.restore']);
    Route::delete('categories_perma_del/{id}', ['uses' => 'Admin\CategoriesController@perma_del', 'as' => 'categories.perma_del']);
   
    // Courses
    Route::delete('courses_mass_destroy', ['uses' => 'Admin\CoursesController@massDestroy', 'as' => 'courses.mass_destroy']);
    Route::post('courses/media', 'Admin\CoursesController@storeMedia')->name('courses.storeMedia');
    Route::post('courses/ckmedia', 'Admin\CoursesController@storeCKEditorImages')->name('courses.storeCKEditorImages');
    Route::resource('courses', 'Admin\CoursesController');
    Route::post('courses_restore/{id}', ['uses' => 'Admin\CoursesController@restore', 'as' => 'courses.restore']);
    Route::delete('courses_perma_del/{id}', ['uses' => 'Admin\CoursesController@perma_del', 'as' => 'courses.perma_del']);

    //lessons
    Route::delete('lessons_mass_destroy', ['uses' => 'Admin\LessonsController@massDestroy', 'as' => 'lessons.mass_destroy']);
    Route::post('lessons/media', 'Admin\LessonsController@storeMedia')->name('lessons.storeMedia');
    Route::post('lessons/ckmedia', 'Admin\LessonsController@storeCKEditorImages')->name('lessons.storeCKEditorImages');
    Route::resource('lessons', 'Admin\LessonsController');
    Route::post('lessons_restore/{id}', ['uses' => 'Admin\LessonsController@restore', 'as' => 'lessons.restore']);
    Route::delete('lessons_perma_del/{id}', ['uses' => 'Admin\LessonsController@perma_del', 'as' => 'lessons.perma_del']);
   
    //media
    Route::post('/spatie/media/upload', 'Admin\SpatieMediaController@create')->name('media.upload');
    Route::post('/spatie/media/remove', 'Admin\SpatieMediaController@destroy')->name('media.remove');

    //quizzes
    Route::resource('quizzes', 'Admin\QuizzesController');
    Route::delete('quizzes_mass_destroy', ['uses' => 'Admin\QuizzesController@massDestroy', 'as' => 'quizzes.mass_destroy']);
    Route::post('quizzes_restore/{id}', ['uses' => 'Admin\QuizzesController@restore', 'as' => 'quizzes.restore']);
    Route::delete('quizzes_perma_del/{id}', ['uses' => 'Admin\QuizzesController@perma_del', 'as' => 'quizzes.perma_del']);
   
    // Questions
    Route::delete('questions/destroy', 'Admin\QuestionsController@massDestroy')->name('questions.massDestroy');
    Route::resource('questions', 'Admin\QuestionsController');
    Route::post('questions_restore/{id}', ['uses' => 'Admin\QuestionsController@restore', 'as' => 'questions.restore']);
    Route::delete('questions_perma_del/{id}', ['uses' => 'Admin\QuestionsController@perma_del', 'as' => 'questions.perma_del']);
   
});